import { CharactersRepository } from "./../repositories/characters.repository";

//casos de uso que necesito en mi aplicación
//cada caso de uso tendrá un único método
export class AllCharactersUseCase {

    async execute() {
        const repository = new CharactersRepository();
        const Characters = await repository.getApiAxio();
        return Characters;
    }

}