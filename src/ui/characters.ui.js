import { LitElement, html } from 'lit';
import './character-detail.ui';
//eventos y los que se dedican a la parte visual 
class CharactersUI extends LitElement {
    static get properties() {
        return {
            Characters: {
                type: Array
            }
        }
    }


    firstUpdated() {
        this.getData();
    }


    sendData() {
        this.dispatchEvent(new CustomEvent('ApiData', {
            detail: { data },
            bubbles: true,
            composed: true
        }))
    }







    /* //metodo que mande la informacion al otro evento

     getData() {
         //promesa
         fetch(this.Characters)
             //necesitamos mandar cuando todo vaya bien y cuando algo falle
             .then((response) => {
                 if (response.ok) return response.json();
                 return Promise.reject(response);
             })
             .then((data) => { this.sendData(data) })
             .catch((error) => { console.warn(error) })




     }*/
    render() {
            return html `
     
         ${this.Characters && this.Characters.map(character => html`
         <div class=card>
            <div class=card-content>
            <h2>${character.name}</h2>
            <img src="${character.img}">
            <p>${character.species} | ${character.status}</p>
            <a href="/character/:${character.id}">El detalle del personaje</a>
            
   
        </div>
        </div>
   
         `)}
      
     `;

     }




}
customElements.define('characters-ui', CharactersUI);