import { LitElement, html } from 'lit';

//eventos y los que se dedican a la parte visual 
class CharactersDetailUI extends LitElement {
    static get properties() {
        return {
            Characters: {
                type: Array

            },
            location: {
                type: Object
            }
        }
    }









    render() {
            return html `
     
         ${this.Characters && this.Characters.map(character => html`
         <div class=card>
            <div class=card-content>
            
            <h2>${character.name}</h2>
            <img src="${character.img}">
            <p>${character.species} | ${character.status}</p>
            <p></p>
            
           
            <a href="/">Volver a home</a>
   
        </div>
        </div>
   
         `)}
      
     `;

     }
     createRenderRoot() {
        return this;
    }




}
customElements.define('characters-detail-ui', CharactersDetailUI);