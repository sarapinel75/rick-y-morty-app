import { Router } from '@vaadin/router';
import './pages/home.page';
import './pages/character.page';
import './pages/character-detail.page';
const outlet = document.getElementById('outlet');
const router = new Router(outlet);
router.setRoutes([
    { path: '/', component: 'home-page' },
    { path: '/character', component: 'character-page' },
    { path: '/character/:characterId', component: 'character-detail-page' },
    { path: '(.*)', redirect: '/' }
])