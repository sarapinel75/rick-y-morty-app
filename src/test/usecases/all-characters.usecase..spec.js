import { CharactersRepository } from "../../repositories/characters.repository";
import { AllCharactersUseCase } from "../../usecases/all-characters.usecase";
import { CHARACTERS } from "../fixtures/characters";
jest.mock("../../repositories/characters.repository")

describe('All  characters use Case', () => {

    beforeEach(() => {
        CharactersRepository.mockClear();
    })

    it('should execute correct', async () => {

        CharactersRepository.mockImplementation(() => {
            return {
                getApiAxio: () => {
                    return CHARACTERS;
                }
            }
        })

        const useCase = new AllCharactersUseCase();
        const Characters = await useCase.execute();

        expect(Characters).toHaveLength(CHARACTERS.length);

    })

})