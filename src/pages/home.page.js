export class HomePage extends HTMLElement {
    constructor() {
        super();
    }
    connectedCallback() {
        this.innerHTML = `

        <style>
        .container1{
            text-align:center;
            margin-top:40px;
        }
        .main{
            font-size: 1.5rem;
                height:60vh;   
                
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
        }
            .main-subtitle{
                    font-size:1.2rem;

            }

            
            .main__link { 
                color: var(--color-dark);
                font-size: 2.2rem;
                font-weight: bold;
                padding: 1rem;
                
           } 

            .main__link:hover { 
                color: var(--color-relaxed);
                font-weight: bold;
                
           } 
           
        
        </style>
            
        <div class=container1>
        <h1 class=main-title2>RICK AND MORTY API</h1>
        
        <main class="main">
        <h2>¿Quieres conocer los personajes de la serie de Rick and Morty?</h2>
        <br>
        <p class="main-subtitle">Para ello haga click aqui</p>
        
            <a class="main__link" href="/character">Empezar</a>
        </main>
       
        </div>

        

            
        `;
    }

}
customElements.define('home-page', HomePage);