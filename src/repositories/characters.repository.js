import axios from "axios";
    //aquí solo llamo y devuelvo los datos que me pasa la api
    //aquí puedo tener n métodos, todos los que necesite para conectar con la api
 
export class CharactersRepository{
    

     async getApiAxio(){

        return await (
              await axios.get(`https://rickandmortyapi.com/api/character`)
        ).data;

        }
        
        
    }

