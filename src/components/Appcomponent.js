import { html, css, LitElement } from "lit";

import { AllCharactersUseCase } from "./../usecases/all-characters.usecase";
import './../ui/characters.ui';



///este componente llama al método que llama a la api
//no pinta los elementos

export class AppRickYMorty extends LitElement {


    static get properties() {
        return {
            Characters: {
                type: Array,

            }

        }

    }
    static get styles() {
        return css `
       
            :host{
                display:block;

            }
            .container{
                text-align:center;
            }   
                  
            characters-ui{
                display:none;
            }
           
             .card{
                margin-top:8%;
                margin-bottom:5%;
                background: #fff;
                border-radius:3px;
                display:inline-block;
                height:300px;
                width: 200px;
                margin:1erm;
                position: relative;
                text-align:center;
                box-shadow: 0 1px 3px rgba(0,0,0, 0.12), 0 1px 2px rgba(0,0,0, 0.24); 
                transition:  all 0.3s cubic-bezier(.25, .8, .25, 1 );

            }
            h2{
                font-size:1.0em;
               
            }
            .title{
                font-size:2rem;
                font-weight:bold;
                color:blue;
                text-shadow :2px 2px green;
            }
            .card:hover{
                box-shadow: 0 14px 28px rgba(0,0,0, 0.12), 0 10px 10px rgba(0,0,0, 0.24); 
            }
            .card img{
                margin-top:10%;
                width:80%;

            }


`;
    }



    async connectedCallback() {
        super.connectedCallback();
        const allCharactersUseCase = new AllCharactersUseCase();
        this.Characters = await allCharactersUseCase.execute();

        this.dataformat(this.Characters);

    }

    /*funcion para dar el formato para mostrar 
        cada una de las tarjetas */
    dataformat(data) {
        //personajes
        let characters = [];
        //sacar los resultados de cada personaje
        data["results"].forEach((character) => {
            //a cada objeto de personajes vamos a estar sacandole la informacion
            characters.push({
                id: character.id,
                img: character.image,
                name: character.name,
                species: character.species,
                status: character.status
            })

        })
        this.Characters = characters;

    }

    render() {
            return html `
            <div class="container">
            <h2 class="title">PERSONAJES</h2>
            </div>
            ${this.Characters && this.Characters.map(character => html`
            <div class=card>
               <div class=card-content>
               <h2>${character.name}</h2>
               <img src="${character.img}">
               <p>${character.species} | ${character.status}</p>
               <a href="/character/${character.id}">El detalle del personaje</a>
       
           </div>
           </div>
        `)}
    `;

    }
    












}
customElements.define('app-rick-morty', AppRickYMorty);

/*customElements.whenDefined("app-element").then(()=> {
    console.log("app element ha sido definido");
})*/