/// <reference types="Cypress" />

it('user click button on home page', () => {

    cy.visit('/', {
        onBeforeLoad(win) {
            cy.stub(win.console, 'log').as('consoleLog')
        }
    });
    cy.get('home-page').shadow().find('a').click();


})